
import java.util.Arrays;
import java.util.List;

public class Producto  implements Comparable <Producto>  {
    //Atributos
    private String nombre;
    private double precio;
    private double litros;
    private String contenido;
    private String unidad_venta;

    //Constrctores
    public Producto (String nombre, double litros, double precio){
        this(nombre,litros,precio,null,null);//sobrecarga
    }

    public Producto (String nombre, String contenido, double precio){
        this(nombre,0,precio,contenido,null);//sobrecarga
    }
    
    public Producto (String nombre,double precio,String unidad_venta){
        this(nombre,0,precio,null,unidad_venta);//sobrecarga
    }

    public Producto(String nombre, double litros, double precio, String contenido, String unidad_venta) {
        this.nombre = nombre;
        if(litros<0){
            this.litros = 0;
        }else{
            this.litros = litros;
        }
        if(precio<0){
            this.precio = 0;
        }else{
            this.precio = precio;
        }
        this.contenido = contenido;
        this.unidad_venta = unidad_venta;
    }
    //Metodos
    public static List crear_productos(){
        Producto huevo = new Producto("Huevo", "12 piezas", 27);
        Producto agua = new Producto("Agua", 5.5, 22);
        Producto papa = new Producto("Papa", 24,"kg");
        Producto leche = new Producto("Leche", 1.5, 29);
        List result = Arrays.asList(huevo,agua,papa,leche);

        return  result;
    }

    public double get_precio() {
        return precio;
    }
    public String get_nombre() {
        return nombre;
    }

    @Override
    public int compareTo(Producto p) {
        if(this.get_precio() < p.get_precio()){
            return -1;
        }
        else if(this.get_precio() > p.get_precio()){
            return 1;
        }else{
            return 0;
        }
    }	

    /**
     * devuelvo el estado del objeto
     */

    @Override
    public String toString(){
        String estado ="";
        if(this.litros !=0){
            estado =  "- Nombre: "+nombre+" /// Litros: "+litros+" /// Precio: $"+precio;
        }
        if(this.contenido != null){
            estado = "- Nombre: "+nombre+" /// Contenido: "+contenido+" /// Precio: $"+precio;
        }
        if(this.unidad_venta != null){
            estado = "- Nombre: "+nombre+" /// Precio: $"+precio+" /// Unidad de venta: "+unidad_venta;
        }

        return estado;
    }
}
