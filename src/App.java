import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) throws Exception {
        List productos= Producto.crear_productos();
        List productos1 = (List) productos.stream().sorted(Comparator.comparing(Producto::get_nombre)).collect(Collectors.toList());
        System.out.println("================================");
        System.out.println("Todos los productos:");
        System.out.println("================================");
        productos1.stream().forEach(producto -> System.out.println(producto.toString()));
        System.out.println("================================");
        System.out.println("Productos de precio menor a $25:");
        System.out.println("================================");
        productos1.stream().filter(p -> ((Producto) p).get_precio() < 25).forEach(p -> System.out.println(p.toString()));
        System.out.println("================================");
        Producto mas_barato = (Producto) productos1.stream().min(Comparator.comparing(Producto::get_precio)).get();
        System.out.println("Producto más barato: "+mas_barato.get_nombre());
        Producto mas_caro = (Producto) productos1.stream().max(Comparator.comparing(Producto::get_precio)).get();
        System.out.println("Producto más caro: "+mas_caro.get_nombre());
        
    }

    
}
